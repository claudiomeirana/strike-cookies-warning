///////////////////////////////////////
// Author: Donatas Stonys            //
// WWW: http://www.BlueWhaleSEO.com  //
// Date: 16 February 2015            //
// Version: 1.0                      //
///////////////////////////////////////

// Edited for customization

// Assign current date to variable
var currentDate = new Date();

// Create some DOM elements
var newCookiesWarningDiv = document.createElement("div");

var cookieMessage =
    "Questo sito raccoglie dati statistici sulla navigazione secondo le norme previste dalla legge. Continuando a navigare accetti il servizio. <span id='readMoreURL'></span>";

// Retrieving cookie's information
function checkCookie(cookieName) {
    "use strict";
    var cookieValue, cookieStartsAt, cookieEndsAt;

    // Get all cookies in one string
    cookieValue = document.cookie;
    // Check if cookie's name is within that string
    cookieStartsAt = cookieValue.indexOf(" " + cookieName + "=");
    if (cookieStartsAt === -1) {
        cookieStartsAt = cookieValue.indexOf(cookieName + "=");
    }
    if (cookieStartsAt === -1) {
        cookieValue = null;
    } else {
        cookieStartsAt = cookieValue.indexOf("=", cookieStartsAt) + 1;
        cookieEndsAt = cookieValue.indexOf(";", cookieStartsAt);

        if (cookieEndsAt === -1) {
            cookieEndsAt = cookieValue.length;
        }

        // Get and return cookie's value
        cookieValue = unescape(
            cookieValue.substring(cookieStartsAt, cookieEndsAt)
        );
        return cookieValue;
    }
}

// Cookie setup function
function setCookie(cookieName, cookieValue, cookiePath, cookieExpires) {
    "use strict";

    // Convert characters that are not text or numbers into hexadecimal equivalent of their value in the Latin-1 character set
    cookieValue = escape(cookieValue);

    // If cookie's expire date is not set
    if (cookieExpires === "") {
        // Default expire date is set to 6 after the current date
        currentDate.setMonth(currentDate.getMonth() + 6);
        // Convert a date to a string, according to universal time (same as GMT)
        cookieExpires = currentDate.toUTCString();
    }

    // If cookie's path value has been passed
    if (cookiePath !== "") {
        cookiePath = ";path = " + cookiePath;
    }

    // Add cookie to visitors computer
    document.cookie =
        cookieName +
        "=" +
        cookieValue +
        ";expires = " +
        cookieExpires +
        cookiePath;

    // Call function to get cookie's information
    checkCookie(cookieName);
}

// Check if cookies are allowed by browser //
function checkCookiesEnabled() {
    "use strict";
    // Try to set temporary cookie
    if (checkCookie("StrCookie") === "Yes") {
        return true;
    } else {
        setCookie("StrCookieExist", "Exist", "", "");
        // If temporary cookie has been set, delete it and return true
        if (checkCookie("StrCookieExist") === "Exist") {
            setCookie("StrCookieExist", "Exist", "", "1 Jan 2000 00:00:00");
            return true;
            // If temporary cookie hasn't been set, return false
        }
        if (checkCookie("StrCookieExist") !== "Exist") {
            return false;
        }
    }
}

// Add HTML form to the website
function acceptCookies() {
    "use strict";

    document
        .getElementById("cookiesWarning")
        .appendChild(newCookiesWarningDiv)
        .setAttribute("id", "cookiesWarningActive");
    document.getElementById("cookiesWarningActive").innerHTML =
        "<div class='cookies-message'>" +
        cookieMessage +
        "<form name='cookieAgreement'><label for='check-agree'><input id='check-agree' type='checkbox' name='agreed' value='Agreed'><span>Accetto i cookie da questo sito</span></label><input type='submit' value='Continua a navigare' onclick='getAgreementValue(); return false;' class='consent-cookies'></form></div>";
    // Change the URL of "Read more..." here
    document.getElementById("readMoreURL").innerHTML =
        "<a href='privacy.php' title='Privacy policy' rel='nofollow'>Privacy</a>";
}

function acceptCookiesTickBoxWarning() {
    "use strict";

    setCookie("StrCookie", "Yes", "", "1 Jan 2000 00:00:00");
    document
        .getElementById("cookiesWarning")
        .appendChild(newCookiesWarningDiv)
        .setAttribute("id", "cookiesWarningActive");
    document.getElementById("cookiesWarningActive").innerHTML =
        "<div class='cookies-message force-agreement'>" +
        cookieMessage +
        "<span id='readMoreURL'></span><form name='cookieAgreement'><label for='check-agree-forced'><input id='check-agree-forced' type='checkbox' name='agreed' value='Agreed'><span>Devi accettare per continuare</span></label><input type='submit' value='Continua a navigare' onclick='getAgreementValue(); return false;' class='consent-cookies'></form></div>";
    // Change the URL of "Read more..." here
    document.getElementById("readMoreURL").innerHTML =
        "<a href='privacy.php' title='Privacy policy' rel='nofollow'>Privacy</a>";
}

// Check if cookie has been set before //
function checkCookieExist() {
    "use strict";

    // Call function to check if cookies are enabled in browser
    if (checkCookiesEnabled()) {
        // If cookies enabled, check if our cookie has been set before and if yes, leave HTML block empty
        if (checkCookie("StrCookie") === "Yes") {
            document.getElementById("cookiesWarning").innerHTML = "";
            // If our cookie hasn't been set before, call cookies' agreement form to HTML block
        } else {
            acceptCookies();
        }
    } else {
        // Display warning if cookies are disabled on browser
        document
            .getElementById("cookiesWarning")
            .appendChild(newCookiesWarningDiv)
            .setAttribute("id", "cookiesWarningActive");
        document.getElementById("cookiesWarningActive").innerHTML =
            "<div class='cookies-message'><span id='cookiesDisabled'>I cookie sono disabilitati, attivali per un servizio migliore.</div>";
    }
}

// Get agreement results
function getAgreementValue() {
    "use strict";

    // If agreement box has been checked, set permanent cookie on visitors computer
    if (document.cookieAgreement.agreed.checked) {
        // Hide agreement form
        document.getElementById("cookiesWarning").innerHTML = "";
        setCookie("StrCookie", "Yes", "", "");
        location.reload();
    } else {
        // If agreement box hasn't been checked, delete cookie (if exist) and add extra warning to HTML form
        acceptCookiesTickBoxWarning();
    }
}
